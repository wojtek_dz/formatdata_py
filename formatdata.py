#!/usr/bin/env python
import getopt
import sys
import json
import re
import xml.etree.ElementTree as xmlet
import string
import xml.dom.minidom
jsonformat = 0
xmlformat = 0
options, restUnused = getopt.getopt(sys.argv[1:], 'jx', ['json', 'xml'])
for opt, unusedArg in options:
	if opt in ['--json', '-j']:
		jsonformat = 1
	if opt in ['--xml', '-x']:
		xmlformat = 1
inp = open('data','r')
outp = dict()
minornr = re.compile('^[^:]*:')
element = re.compile('\s[a-zA-Z]:[^,^\s]*[\s,]')
for line in inp:
	mn = minornr.findall(line)[0]
	mn = mn.strip(':'+string.whitespace)
	outp[mn] = dict()
	elements = element.findall(line)
	for e in elements:
		e = e.strip(','+string.whitespace)
		ePair = e.split(':')
		outp[mn][ePair[0]] = ePair[1]
if xmlformat:
	root = xmlet.Element('program')
	for k in outp.keys():
		mn = xmlet.SubElement(root, 'minor' )
		mn.attrib={'nr':k}
		for e in outp[k].keys():
			leaf = xmlet.SubElement(mn,e)
			leaf.text = outp[k][e]
	uglyOutp = xmlet.tostring(root)
	reparsed = xml.dom.minidom.parseString(uglyOutp)
	print (reparsed.toprettyxml(indent=" "))
if jsonformat:
	print (json.dumps(outp, sort_keys=True, indent=4 ))

