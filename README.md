This simple program takes the contents of a file called "data", which has a format like this:

line number: letter:string, letter:string, ... letter:string

example:

0: a:1, x:2, w:3
1: b:1, c:3, t:5
2: p:32a, d:k00%


And prints it in json or xml format, according to arguments provided upon execution (--json or --xml).
